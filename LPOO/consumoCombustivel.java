import java.text.DecimalFormat;
import java.util.Scanner;

class Main {
    public static void main(String args[]){
        Scanner ler = new Scanner(System.in);
        int d=0;
        double l=0;
        d=ler.nextInt();
        l=ler.nextDouble();
        double kl= d/l;
        DecimalFormat df = new DecimalFormat("#0.000");
        System.out.println(df.format(kl)+" km/l");
    }
}