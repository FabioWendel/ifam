
import java.util.Scanner;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;

class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("##.00");
        double vet[] = new double[3];
        double num;
        int i;
        
        while(true){
            num = in.nextDouble();
            if(num==0){
                 break;
            }
            for(i=0;i<3;i++){  
                Arrays.sort(vet);
                if(num>vet[i]){
                    vet[i] = num;
                    break;
                }
            }
            
        }//fim while
        
        for(i=2;i>=0;i--){  
             System.out.println(df.format(vet[i]));  
        }
        
    }
}
