/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Q2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class Aluno {

    private String matricula;
    private String nome;
    private List<Double> listaNotas = new ArrayList<>();

    public Aluno(String matricula, String nome) {
        this.matricula = matricula;
        this.nome = nome;
    }

    public void incluirNota(double nota) {
        listaNotas.add(nota);
    }

    public void listarNotas() {
        for (Double notas : listaNotas) {
            System.out.println(notas);
        }
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Double> getListaNotas() {
        return listaNotas;
    }

    public void setListaNotas(List<Double> listaNotas) {
        this.listaNotas = listaNotas;
    }
}
