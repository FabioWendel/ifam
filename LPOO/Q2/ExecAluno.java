/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Q2;

import java.util.Scanner;

/**
 *
 * @author Aluno
 */
public class ExecAluno {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        Aluno aluno = new Aluno("111222", "fabio wendel");

        System.out.println("\nInserir nota: \n");
        aluno.incluirNota(scan.nextDouble());
        aluno.incluirNota(scan.nextDouble());
        aluno.incluirNota(scan.nextDouble());

        System.out.println("\nNotas:");
        aluno.listarNotas();

    }
}
