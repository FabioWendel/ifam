/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package P1Q6;

import java.util.Scanner;

/**
 *
 * @author Aluno
 */
public class ExecItemCompra {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        ItemCompra item = new ItemCompra();

        System.out.println("\nDigite o ID do produto:");
        item.setIdProduto(scan.nextInt());
        System.out.println("\nDigite a quantidade de produto:");
        item.setQuantidade(scan.nextInt());
        System.out.println("\nDigite o valor do seu produto:");
        item.setValor(scan.nextDouble());

        System.out.println("\nID produto:" + item.getIdProduto()
                + "\nValor total da compra é : " + item.calcularValorTotal());
    }
}
