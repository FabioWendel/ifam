/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package P1Q6;

/**
 *
 * @author Aluno
 */
public class ItemCompra {

    private int idProduto;
    private double valor;
    private int quantidade;

    public int getIdProduto() {
        return idProduto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public double calcularValorTotal() {

        return this.quantidade * this.valor;

    }
}
