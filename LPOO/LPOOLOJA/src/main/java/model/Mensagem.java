/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import arquivo.Arquivo;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author fabio
 */
public class Mensagem {

    private int id;
    private String texto;
    private boolean status;
    private static int ultimo;
    private static final String caminho = "ListaDeMensagem.txt";

    Mensagem mensagem;

    public Mensagem() {
    }

    public Mensagem(int id, String texto, boolean status) {
        this.id = id;
        this.texto = texto;
        this.status = status;
    }

    public Mensagem(JSONObject json) {
        this.id = json.getInt("id");
        this.texto = json.getString("texto");
        this.status = json.getBoolean("status");
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("id", this.id);
        json.put("texto", this.texto);
        json.put("status", this.status);
        return json;
    }
        public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public static int getUltimo() {
        return ultimo;
    }

    public static void setUltimo(int ultimo) {
        Mensagem.ultimo = ultimo;
    }

    public boolean cadastrar(int row) {
        JSONObject json = this.toJson();

        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        if (row >= 0) {
            jA.put(row, json);
        } else {
            jA.put(json);
        }

        Arquivo.Write(caminho, jA.toString());

        return true;
    }

    public boolean excluir(int row) {
        //JSONObject json = this.toJson();
        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        jA.remove(row);
        Arquivo.Write(caminho, jA.toString());

        return true;

    }


    public ArrayList<Mensagem> getMensagem() {
        ArrayList<Mensagem> listaMensagem = new ArrayList();
        String base = Arquivo.Read(caminho);
        if (base.isEmpty() || base.length() < 5) {
            return null;
        }
        JSONArray jA = new JSONArray(base);
        for (int i = 0; i < jA.length(); i++) {
            Mensagem A = new Mensagem(jA.getJSONObject(i));
            listaMensagem.add(A);
            ultimo = jA.getJSONObject(i).getInt("id");
        }
        return listaMensagem;
    }
}
