/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import arquivo.Arquivo;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author fabio
 */
public class Compra {
    
    private int id;
    private int idCliente;
    private ArrayList<Integer> quantidade;
    private double total;
    private ArrayList<Integer> idProduto;
    private boolean status = false;
    private static String caminho = "ListaCompra.txt";

//    public Compra(int id, ArrayList<Integer> quantidade, double total) {
//        this.id = id;
//        this.quantidade = quantidade;
//        this.total = total;
//    }

    public Compra() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    
    public ArrayList<Integer> getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(ArrayList<Integer> quantidade) {
        this.quantidade = quantidade;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public static String getCaminho() {
        return caminho;
    }

    public static void setCaminho(String caminho) {
        Compra.caminho = caminho;
    }

    public ArrayList<Integer> getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(ArrayList<Integer> idProduto) {
        this.idProduto = idProduto;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    
    
    
    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("id", this.id);
        json.put("idCliente", this.id);
        json.put("idProduto", this.idProduto);
        json.put("quantidade", this.quantidade);
        json.put("total", this.total);
        json.put("status", this.status);
        return json;
    }
    
    public Compra(JSONObject json) {
        this.id = json.getInt("id");
        this.idCliente = json.getInt("idCliente");
        JSONArray temporariaProduto = json.getJSONArray("idProduto");
        JSONArray temporariaQuantidade = json.getJSONArray("quantidade");
        this.total = json.getDouble("total");
        this.status = json.getBoolean("status");
    }    
    public boolean adicionarProduto(int row) {
        JSONObject json = this.toJson();

        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        if (row >= 0) {;
            jA.put(row, json);
        } else {
            jA.put(json);
        }

        Arquivo.Write(caminho, jA.toString());

        return true;
    }

    
    public boolean removerProduto(int row) {
        //JSONObject json = this.toJson();
        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        jA.remove(row);
        Arquivo.Write(caminho, jA.toString());

        return true;

    }
    
    public ArrayList<Compra> listarCarrinho() {
        ArrayList<Compra> listaCompra = new ArrayList();
        String base = Arquivo.Read(caminho);
        if (base.isEmpty() || base.length() < 5) {
            return null;
        }
        JSONArray jA = new JSONArray(base);

        for (int i = 0; i < jA.length(); i++) {
            Compra ic = new Compra(jA.getJSONObject(i));
            listaCompra.add(ic);
            //ultimo = jA.getJSONObject(i).getInt("id");
        }
        return listaCompra;

    }


    
}
