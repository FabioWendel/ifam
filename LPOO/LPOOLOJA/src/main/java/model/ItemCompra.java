/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import arquivo.Arquivo;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author fabio
 */
public class ItemCompra {

    private int id;
    private int cliente;
    private boolean status = false;
    private ArrayList<Integer> qtdProduto;
    private ArrayList<Integer> produtoId;
    private static String caminho = "ListaCompra.txt";

    public ItemCompra() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Integer> getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(ArrayList<Integer> produtoId) {
        this.produtoId = produtoId;
    }

    public ArrayList<Integer> getQtdProduto() {
        return qtdProduto;
    }

    public void setQtdProduto(ArrayList<Integer> qtdProduto) {
        this.qtdProduto = qtdProduto;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("idCompra", this.id);
        json.put("idCliente", this.cliente);
        json.put("Produto", this.produtoId);
        json.put("Quantidade", this.qtdProduto);
        json.put("Status", this.status);

        return json;
    }

    public ItemCompra(JSONObject json) {
        this.id = json.getInt("idCompra");
        this.cliente = json.getInt("idCliente");
        JSONArray tempProduto = json.getJSONArray("Produto");
        JSONArray tempQtd = json.getJSONArray("Quantidade");
        this.status = json.getBoolean("Status");
        this.produtoId = new ArrayList();
        this.qtdProduto = new ArrayList();
        for (int i = 0; i < tempProduto.length(); i++) {
            this.produtoId.add(Integer.parseInt(tempProduto.get(i).toString()));
            this.qtdProduto.add(Integer.parseInt(tempQtd.get(i).toString()));
        }

    }

    public boolean cadastrar(int row) {
        JSONObject json = this.toJson();

        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        if (row >= 0) {;
            jA.put(row, json);
        } else {
            jA.put(json);
        }

        Arquivo.Write(caminho, jA.toString());

        return true;
    }

    public ArrayList<ItemCompra> getCompras() {
        ArrayList<ItemCompra> listaCompra = new ArrayList();
        String base = Arquivo.Read(caminho);
        if (base.isEmpty() || base.length() < 5) {
            return null;
        }
        JSONArray jA = new JSONArray(base);

        for (int i = 0; i < jA.length(); i++) {
            ItemCompra ic = new ItemCompra(jA.getJSONObject(i));
            listaCompra.add(ic);
        }
        return listaCompra;

    }

    public static boolean excluir(int row) {
        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        jA.remove(row);
        Arquivo.Write(caminho, jA.toString());

        return true;

    }

}
