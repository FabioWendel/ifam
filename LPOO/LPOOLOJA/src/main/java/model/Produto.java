/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import arquivo.Arquivo;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author fabio
 */
public class Produto {

    private int id;
    private String nome;
    private String tipo;
    private double preco;
    private double estoque;
    private static int ultimo;
    private static final String caminho = "ListaDeProduto.txt";
    private Produto produto;

    public Produto(int id, String nome, String tipo, double preco, double estoque) {
        this.id = id;
        this.nome = nome;
        this.tipo = tipo;
        this.preco = preco;
        this.estoque = estoque;
    }

    public Produto() {
    }

    public Produto(JSONObject json) {
        this.id = json.getInt("id");
        this.nome = json.getString("nome");
        this.tipo = json.getString("tipo");
        this.preco = json.getDouble("preco");
        this.estoque = json.getInt("estoque");
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("id", this.id);
        json.put("nome", this.nome);
        json.put("tipo", this.tipo);
        json.put("preco", this.preco);
        json.put("estoque", this.estoque);
        return json;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getEstoque() {
        return estoque;
    }

    public void setEstoque(double estoque) {
        this.estoque = estoque;
    }

    public static int getUltimo() {
        return ultimo;
    }

    public static void setUltimo(int ultimo) {
        Produto.ultimo = ultimo;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean cadastrar(int row) {
        JSONObject json = this.toJson();

        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        if (row >= 0) {
            jA.put(row, json);
        } else {
            jA.put(json);
        }

        Arquivo.Write(caminho, jA.toString());

        return true;
    }

    public boolean excluir(int row) {
        //JSONObject json = this.toJson();
        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        jA.remove(row);
        Arquivo.Write(caminho, jA.toString());

        return true;

    }

    public ArrayList<Produto> getProdutos() {
        ArrayList<Produto> listaProds = new ArrayList();
        String base = Arquivo.Read(caminho);
        if (base.isEmpty() || base.length() < 5) {
            return null;
        }
        JSONArray jA = new JSONArray(base);
        for (int i = 0; i < jA.length(); i++) {
            Produto A = new Produto(jA.getJSONObject(i));
            listaProds.add(A);
            ultimo = jA.getJSONObject(i).getInt("id");
        }
        return listaProds;
    }
    
        public boolean alterar(int row) {
        JSONObject json = this.toJson();

        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        if (row >= 0) {
            jA.put(row, json);
        } else {
            jA.put(json);
        }

        Arquivo.Write(caminho, jA.toString());

        return true;
    }

}
