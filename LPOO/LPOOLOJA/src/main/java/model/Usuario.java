/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import arquivo.Arquivo;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author fabio
 */
public class Usuario {

    private int id;
    private String nome;
    private String cpf;
    private String rg;
    private String email;
    private String senha;
    private boolean administrador;
    private static int ultimo;
    private String caminho = "ListaDeCliente.txt";
    private Usuario user;

    public Usuario() {

    }

    public Usuario(int id, String nome, String cpf, String rg, String email, String senha, boolean administrador) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.email = email;
        this.senha = senha;
        this.administrador = administrador;
    }

    public Usuario(JSONObject json) {
        this.id = json.getInt("id");
        this.nome = json.getString("nome");
        this.cpf = json.getString("cpf");
        this.rg = json.getString("rg");
        this.email = json.getString("email");
        this.senha = json.getString("senha");
        this.administrador = json.getBoolean("administrador");
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("id", this.id);
        json.put("nome", this.nome);
        json.put("cpf", this.cpf);
        json.put("rg", this.rg);
        json.put("email", this.email);
        json.put("senha", this.senha);
        json.put("administrador", this.administrador);
        return json;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public static int getUltimo() {
        return ultimo;
    }

    public static void setUltimo(int ultimo) {
        Usuario.ultimo = ultimo;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }
    
    public boolean cadastrar(int row) {

        JSONObject json = this.toJson();

        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        if (row >= 0) {
            jA.put(row, json);
        } else {
            jA.put(json);
        }

        Arquivo.Write(caminho, jA.toString());

        return true;
    }
    


    public boolean excluir(int row) {
        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        jA.remove(row);
        Arquivo.Write(caminho, jA.toString());

        return true;

    }
    public ArrayList<Usuario> getUsuario() {
        ArrayList<Usuario> listaUsuario = new ArrayList();
        String base = Arquivo.Read(caminho);
        if (base.isEmpty() || base.length() < 5) {

            return null;
        }
        JSONArray jA = new JSONArray(base);
        for (int i = 0; i < jA.length(); i++) {
            Usuario A = new Usuario(jA.getJSONObject(i));
            listaUsuario.add(A);
            ultimo = jA.getJSONObject(i).getInt("id");
        }
        return listaUsuario;
    }
    
        public boolean alterarDadosPessoais(int row) {

        JSONObject json = this.toJson();

        String base = Arquivo.Read(caminho);
        JSONArray jA = new JSONArray();
        if (!base.isEmpty() && base.length() > 5) {
            jA = new JSONArray(base);
        }
        if (row >= 0) {
            jA.put(row, json);
        } else {
            jA.put(json);
        }

        Arquivo.Write(caminho, jA.toString());

        return true;
    }
}
