/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.Categoria;

/**
 *
 * @author fabio
 */
public class CategoriaControl {

    public int salvarCategoria(int row, String id, String tipo) {
        if (tipo.equals("")) {
            return 1;
        }
        Categoria c1 = new Categoria();
        ///if(id.equals("") || nome.equals(""))
        c1.setId(Integer.parseInt(id));
        c1.setTipo(tipo);
        c1.cadastrar(row);
        return 0;
    }

    Categoria categoria = new Categoria();

    public ArrayList<String[]> getsCategorias() {
        ArrayList<String[]> arrayCat = new ArrayList();
        ArrayList<Categoria> aCa = categoria.getCategs();
        if (aCa != null) {
            for (int i = 0; i < aCa.size(); i++) {
                String ac[] = new String[2];
                ac[0] = Integer.toString(aCa.get(i).getId());
                ac[1] = aCa.get(i).getTipo();
                arrayCat.add(ac);
            }
        }
        return arrayCat;

    }

    public boolean excluirCategoria(int row) {
        return categoria.excluir(row);

    }
//    
//
//    public int buscarCategoriaTipo(String tipo) {
//        return categoria.buscarTipo(tipo);
//    }

    public int novoId() {
        ArrayList a = categoria.getCategs();
//        int i = a.size();
        return Categoria.getUltimo();   
    }
}
