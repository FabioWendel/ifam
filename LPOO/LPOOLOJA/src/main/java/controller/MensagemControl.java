/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.Mensagem;

/**
 *
 * @author fabio
 */
public class MensagemControl {

    Mensagem mensagem = new Mensagem();

    public int salvarMsg(int row, String id, String texto, boolean status) {
        if (texto.equals("")) {
//       
            return 1;
        }
        Mensagem a1 = new Mensagem();

        ///if(id.equals("") || nome.equals(""))
        a1.setId(Integer.parseInt(id));
        a1.setStatus(status);
        a1.setTexto(texto);

        a1.cadastrar(row);
        return 0;
    }

    public ArrayList<String[]> getMsg() {
        ArrayList<String[]> Admins = new ArrayList();
        ArrayList<Mensagem> a1 = mensagem.getMensagem();
        if (a1 != null) {
            for (int i = 0; i < a1.size(); i++) {
                String a[] = new String[3];
                a[0] = Integer.toString(a1.get(i).getId());
                a[1] = a1.get(i).getTexto();
                if (a1.get(i).isStatus() == false) {
                    a[2] = "Pendente";
                } else {
                    a[2] = "Visto";
                }
                Admins.add(a);
            }
        }
        return Admins;

    }

    public boolean excluirMsg(int row) {
        return mensagem.excluir(row);

    }

    public int novoId() {
        return mensagem.getUltimo();
    }
}
