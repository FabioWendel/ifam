/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import view.CadastroCategoria;
import view.CadastroCliente;
import view.CadastroMensagem;
import view.CadastroProduto;
import view.CadastroUsuario;
import view.TabelaVIewMsgAdministrador;
import view.TabelaViewAdministrador;
import view.TabelaViewCategoria;
import view.TabelaViewCompraRealizada;
import view.TabelaViewProduto;
import view.TabelaViewUsuario;

/**
 *
 * @author fabio
 */
public class MenuAdministradorControl {
    
    public void telaCompras(){
        TabelaViewCompraRealizada comp = new TabelaViewCompraRealizada();
        comp.setVisible(true);
    }

    public void telaAdministrador() {
        TabelaViewAdministrador adm = new TabelaViewAdministrador();
        adm.setVisible(true);
    }

    public void telaProduto() {
        TabelaViewProduto produto = new TabelaViewProduto();
        produto.setVisible(true);
    }

    public void telaCategoria() {
        TabelaViewCategoria categoria = new TabelaViewCategoria();
        categoria.setVisible(true);
    }

    public void telaCliente() {
        TabelaViewUsuario cliente = new TabelaViewUsuario();
        cliente.setVisible(true);
    }

    public void TelaMensagem() {
        TabelaVIewMsgAdministrador adm = new TabelaVIewMsgAdministrador();
        adm.setVisible(true);
    }

    public void CadtelaAdministrador() {
        CadastroUsuario adm = new CadastroUsuario();
        adm.setVisible(true);
    }

    public void CadtelaProduto() {
        CadastroProduto produto = new CadastroProduto();
        produto.setVisible(true);
    }

    public void CadtelaCategoria() {
        CadastroCategoria categoria = new CadastroCategoria();
        categoria.setVisible(true);
    }

    public void CadtelaCliente() {
        CadastroCliente cliente = new CadastroCliente();
        cliente.setVisible(true);
    }

    public void CadTelaMensagem() {
        CadastroMensagem adm = new CadastroMensagem();
        adm.setVisible(true);
    }
}
