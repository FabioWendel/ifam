/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.Usuario;

/**
 *
 * @author fabio
 */
public class UsuarioControl {

    private Usuario user = new Usuario();

    public int salvarUsuario(int row, String id, String nome, String cpf, String rg, String email, String senha, boolean administrador) {

        if (nome.equals("") || cpf.equals("") || rg.equals("") || email.equals("") || senha.equals("")) {
            return 1;
        }

        Usuario usuario = new Usuario();
        usuario.setId(Integer.parseInt(id));
        usuario.setNome(nome);
        usuario.setCpf(cpf);
        usuario.setRg(rg);
        usuario.setEmail(email);
        usuario.setSenha(senha);
        usuario.setAdministrador(administrador);
        usuario.cadastrar(row);
        return 0;
    }
        public int salvarCliente(int row, String id, String nome, String cpf, String rg, String email, String senha) {

        if (nome.equals("") || cpf.equals("") || rg.equals("") || email.equals("") || senha.equals("")) {
            return 1;
        }

        Usuario usuario = new Usuario();
        usuario.setId(Integer.parseInt(id));
        usuario.setNome(nome);
        usuario.setCpf(cpf);
        usuario.setRg(rg);
        usuario.setEmail(email);
        usuario.setSenha(senha);
        usuario.cadastrar(row);
        return 0;
    }

    public ArrayList<String[]> getUsuarios() {
        ArrayList<String[]> Admins = new ArrayList();
        ArrayList<Usuario> a1 = user.getUsuario();
        
        if (a1 != null) {
            for (int i = 0; i < a1.size(); i++) {
                String a[] = new String[10];
                a[0] = Integer.toString(a1.get(i).getId());
                a[1] = a1.get(i).getNome();
                a[2] = a1.get(i).getCpf();
                a[3] = a1.get(i).getRg();
                a[4] = a1.get(i).getEmail();
                a[5] = a1.get(i).getSenha();
                a[6] = Boolean.toString(a1.get(i).isAdministrador());
                Admins.add(a);
            }
        }
        return Admins;
    }
    
        public ArrayList<String[]> getAdm() {
        ArrayList<String[]> Admins = new ArrayList();
        ArrayList<Usuario> a1 = user.getUsuario();
        
        if (a1 != null) {
            for (int i = 0; i < a1.size(); i++) {
                
                System.out.println();
             if(LoginControl.isGlobalControl()){   
                String a[] = new String[10];
                a[0] = Integer.toString(a1.get(i).getId());
                a[1] = a1.get(i).getNome();
                a[2] = a1.get(i).getCpf();
                a[3] = a1.get(i).getRg();
                a[4] = a1.get(i).getEmail();
                a[5] = a1.get(i).getSenha();
                a[6] = Boolean.toString(a1.get(i).isAdministrador());
                Admins.add(a);
             }
            }
        }
        return Admins;
    }

    public boolean excluir(int row) {
        return user.excluir(row);

    }

    public int novoId() {
        ArrayList<Usuario> a1 = user.getUsuario();
        return Usuario.getUltimo();
//        
    }
}
