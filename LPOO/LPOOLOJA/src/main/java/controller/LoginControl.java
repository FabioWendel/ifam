/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.Usuario;
import view.CadastroCliente;
import view.CadastroUsuario;
import view.MenuViewAdministrador;
import view.MenuViewCliente;

/**
 *
 * @author fabio
 */
public class  LoginControl {
    
    private static boolean globalControl;

    public static boolean isGlobalControl() {
        return globalControl;
    }

    public void setGlobalControl(boolean globalControl) {
        this.globalControl = globalControl;
    }
    
    
    public boolean verificarLoginAdmin(String nome, String senha) {
        Usuario usuario = new Usuario();

        ArrayList<Usuario> a1 = usuario.getUsuario();

        for (Usuario a : a1) {
            if (a.getNome().equals(nome) && a.getSenha().equals(senha)) {

                if (a.isAdministrador()) {
                    setGlobalControl(a.isAdministrador());
                    System.out.println(isGlobalControl());
                    MenuViewAdministrador menu = new MenuViewAdministrador();
                    menu.setVisible(true);
                    return true;
                } else {
                    //globalControl = false;
                    MenuViewCliente menu = new MenuViewCliente();
                    menu.setVisible(true);
                    return true;
                }

            }
        }
        return false;

    }

    public void bt_cadastrar() {
        CadastroCliente cad = new CadastroCliente();
        cad.setVisible(true);
    }

}
