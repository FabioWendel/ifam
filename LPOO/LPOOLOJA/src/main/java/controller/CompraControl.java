/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.Compra;
import model.ItemCompra;
import model.Produto;

/**
 *
 * @author fabio
 */
public class CompraControl {

    Compra compra = new Compra();
    Produto produto = new Produto();

    public ArrayList<String[]> getProdutos() {
        ArrayList<String[]> Prods = new ArrayList();
        ArrayList<Produto> a1 = produto.getProdutos();
        if (a1 != null) {
            for (int i = 0; i < a1.size(); i++) {
                String a[] = new String[10];
                a[0] = Integer.toString(a1.get(i).getId());
                a[1] = a1.get(i).getTipo();
                a[2] = Double.toString(a1.get(i).getPreco());
                a[3] = Double.toString(a1.get(i).getEstoque());
                Prods.add(a);
            }
        }
        return Prods;

    }
    public boolean salvarCompra(int row, String id, int idCliente, ArrayList<Integer> produto, ArrayList<String> qtd) {
        ItemCompra c1 = new ItemCompra();
        c1.setId(Integer.parseInt(id));
        c1.setCliente(idCliente);
        ArrayList<Integer> a = new ArrayList();
        ArrayList<Integer> b = new ArrayList();
        for (int i = 0; i < qtd.size(); i++) {
            b.add(Integer.parseInt(qtd.get(i)));
        }
        c1.setProdutoId(produto);
        c1.setQtdProduto(b);
        c1.cadastrar(row);
        return true;
    }

    public ArrayList<String[]> getCompras() {
        ArrayList<Compra> listaC = compra.listarCarrinho();
        ArrayList<String[]> getC = new ArrayList();
        if (listaC != null) {
            for (Compra c : listaC) {
                String qtd = "";
                String pr = "";
                String nome = String.valueOf(c.getIdCliente());
                String idCompra = String.valueOf(c.getId());
                qtd = "[";
                pr = "[";
                for (int i = 0; i < c.getIdProduto().size(); i++) {
                    qtd += String.valueOf(c.getQuantidade().get(i)) + ", ";
                    pr += String.valueOf(c.getIdProduto().get(i)) + ", ";
                }
                qtd += "]";
                pr += "]";
                String co[] = new String[5];
                co[0] = idCompra;
                co[1] = nome;
                co[2] = pr;
                co[3] = qtd;

                if (c.isStatus() == true) {
                    co[4] = "Confirmado";
                } else {
                    co[4] = "Pendente";
                }

                getC.add(co);

            }
        }
        return getC;
    }

    public void atualizarStatus(int row, boolean status) {
        ArrayList<Compra> iC = compra.listarCarrinho();
        iC.get(row).setStatus(status);
        iC.get(row).adicionarProduto(row);
    }
}
