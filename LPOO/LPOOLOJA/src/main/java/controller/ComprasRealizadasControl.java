/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Compra;
import model.ItemCompra;

/**
 *
 * @author fabio
 */
public class ComprasRealizadasControl {

    public static ArrayList<String[]> getCompras() {
        ItemCompra item = new ItemCompra();

        ArrayList<ItemCompra> listaC = item.getCompras();
        ArrayList<String[]> getC = new ArrayList();
        if (listaC != null) {
            for (ItemCompra c : listaC) {
                String qtd = "";
                String pr = "";
                String nome = String.valueOf(c.getCliente());
                String idCompra = String.valueOf(c.getId());
                qtd = "[";
                pr = "[";
                for (int i = 0; i < c.getProdutoId().size(); i++) {
                    qtd += String.valueOf(c.getProdutoId().get(i)) + ", ";
                    pr += String.valueOf(c.getQtdProduto().get(i)) + ", ";
                }
                qtd += "]";
                pr += "]";
                String co[] = new String[5];
                co[0] = idCompra;
                co[1] = nome;
                co[2] = pr;
                co[3] = qtd;

                if (c.isStatus() == true) {
                    co[4] = "Confirmado";
                } else {
                    co[4] = "Pendente";
                }

                getC.add(co);

            }

        }
        return getC;
    }

}
