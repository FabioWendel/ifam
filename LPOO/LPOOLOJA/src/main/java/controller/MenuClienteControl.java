/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.swing.JOptionPane;
import model.Usuario;
import view.CadastroMensagem;
import view.CadastroUsuario;
import view.CompraView;

/**
 *
 * @author fabio
 */
public class MenuClienteControl {

    public int indexCliente;
    Usuario usuario = new Usuario();

    public MenuClienteControl() {

    }

    public MenuClienteControl(int i) {
        indexCliente = i;
    }

    public boolean bt_Mensagem() {
        CadastroMensagem msg = new CadastroMensagem();
        msg.setVisible(true);
        return true;
    }

    public boolean bt_Compra() {
        CompraView CW = new CompraView(indexCliente + 1);
        CW.setVisible(true);
        return true;
    }

    public boolean bt_Editar() {
        CadastroUsuario cad = new CadastroUsuario(indexCliente);
        cad.setVisible(true);

        return true;
    }

    public boolean bt_Excluir() {

        int resp = JOptionPane.showConfirmDialog(null, "Voce deseja realmente excluir sua conta");
        if (resp == 0) {
            JOptionPane.showMessageDialog(null, "Conta deletada");
            return usuario.excluir(indexCliente);

        } else {
            JOptionPane.showMessageDialog(null, "Operação cancelada");
            return true;
        }

    }
}
