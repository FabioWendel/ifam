/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.Categoria;
import model.Produto;

/**
 *
 * @author fabio
 */
public class ProdutoControl {

    Produto produto = new Produto();

    public int salvarProduto(int row, String id, String nome, String tipo, String preco, String estoque) {
        if (tipo.equals("") || estoque.equals("")) {
            return 1;
        }
        Produto a1 = new Produto();
        a1.setId(Integer.parseInt(id));
        a1.setNome(nome);
        a1.setTipo(tipo);
        a1.setPreco(Double.parseDouble(preco));
        a1.setEstoque(Double.parseDouble(estoque));
        a1.cadastrar(row);
        return 0;
    }

    public ArrayList<String[]> getProdutos() {
        ArrayList<String[]> Prods = new ArrayList();
        ArrayList<Produto> a1 = produto.getProdutos();
        if (a1 != null) {
            for (int i = 0; i < a1.size(); i++) {
                String a[] = new String[10];
                a[0] = Integer.toString(a1.get(i).getId());
                a[1] = a1.get(i).getNome();
                a[2] = a1.get(i).getTipo();
                a[3] = Double.toString(a1.get(i).getPreco());
                a[4] = Double.toString(a1.get(i).getEstoque());
                Prods.add(a);
            }
        }
        return Prods;

    }

    public boolean excluirProduto(int row) {
        return produto.excluir(row);

    }

    Categoria categoria = new Categoria();

    public ArrayList<String> getTipos() {
        ArrayList<Categoria> c = categoria.getCategs();
        ArrayList<String> L = new ArrayList();
        if (c != null) {
            for (Categoria c1 : c) {
                L.add(c1.getTipo());
            }
        }

        return L;

    }

    public int novoId() {
        ArrayList a = produto.getProdutos();
        int i = a.size();
        return Produto.getUltimo();
    }
}
