import java.text.DecimalFormat;
import java.util.Scanner;

class Main {
    public static void main(String args[]){
        Scanner ler = new Scanner(System.in);
        int id,qtd;
        double valor;
        DecimalFormat df = new DecimalFormat("#0.00");
        id = ler.nextInt();
        qtd = ler.nextInt();
        if (id==1){
            valor = 4.0*qtd;
            System.out.println("Total: R$ "+df.format(valor));
            return;
        }
        if (id==2){
            valor = 4.5*qtd;
            System.out.println("Total: R$ "+df.format(valor));
            return;
        }
        if (id==3){
            valor = 5.0*qtd;
            System.out.println("Total: R$ "+df.format(valor));
            return;
        }
        if (id==4){
            valor = 2.0*qtd;
            System.out.println("Total: R$ "+df.format(valor));
            return;
        }
        if (id==5){
            valor = 1.5*qtd;
            System.out.println("Total: R$ "+df.format(valor));
            return;
        }
    }
}
