import java.text.DecimalFormat;
import java.util.Scanner;

class Main {
    public static void main(String args[]){
        Scanner ler = new Scanner(System.in);
        int v,d,a,m;
        v =ler.nextInt();
        a = v/365;
        m = (v%365)/30;
        d = (v%365)%30;
        System.out.println(a+" ano(s)");
        System.out.println(m+" mes(es)");
        System.out.println(d+" dia(s)");
    }
}
