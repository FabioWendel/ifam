import java.text.DecimalFormat;
import java.util.Scanner;

class Main {
    public static void main(String [] args){
        Scanner ler = new Scanner(System.in);
        String nome;
        double salario,vendas,taxacomi,safinal;
        nome = ler.next();
        salario = ler.nextDouble();
        vendas = ler.nextDouble();
        taxacomi = vendas*0.15;
        safinal = salario+taxacomi;
        DecimalFormat df = new DecimalFormat("#0.00");
        System.out.println("TOTAL = R$ "+df.format(safinal));
    }
}
