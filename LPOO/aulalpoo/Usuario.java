
package aulalpoo;

import java.util.ArrayList;
import java.util.List;

public class Usuario {
    private String nome;
    private String email;
    private String telefone;
    private List<Usuario> contatos;

    public Usuario(String nome, String email, String telefone) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        contatos = new ArrayList();
    }
    
    
    public boolean adicionarContato(Usuario contatos){
       if(!possuiContato(contatos)){
           this.contatos.add(contatos);
           return true;
       }else{
           return false;
       }
    }
    
    
    public void listaContato(){
        contatos.forEach((item)->{
        System.out.println(item);
        });
    }
    
    public boolean possuiContato(Usuario contatos){
        if(!this.contatos.equals(contatos)){
            System.out.println("Contato existente");
            return true;
        }else{
            return false;
        }
    }
    
}
