
package aulalpoo;

import java.util.ArrayList;
import java.util.List;

public class BasicList {
    public static void main(String[] args) {
        
        List<Integer> listaNumeros = new ArrayList();
        
        listaNumeros.add(101);
        listaNumeros.add(102);
        
        System.out.println("Tamanho Lista: " + listaNumeros.size());
        
        
               
        listaNumeros.forEach((item)->{
            System.out.println(item);
        });
        
        int count = 0;
        for (Integer numero : listaNumeros){
            count += numero;
        }
        System.out.println("Total: "+ count);
        
        
        if(listaNumeros.contains(100)){
            System.out.println("100 presente");
        }else{
            System.out.println("100 ausente");
        }
    }
}
