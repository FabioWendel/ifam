package app;

import java.util.Scanner;

public class ExecEditorDeTexto {
    public static void main(String[] args) {

        String path = "/home/fabio/ArquivosLPOO/";
        Scanner scan = new Scanner(System.in);
        String texto = ".txt";
        String nomeArq;
        String deleteArq;
        String conteudo;

        EditorDeTexto.ListContent(path);

        System.out.println("\nDigite o nome do seu arquivo:\n");
        nomeArq = scan.nextLine();

        boolean criou = EditorDeTexto.createFile(path + nomeArq + texto);

        if (criou) {
            System.out.println("\nArquivo criado!\n");

            System.out.println("\nDigite o conteudo desejado:\n");

            conteudo = scan.nextLine();

            boolean escreveu = EditorDeTexto.writeFile(path + nomeArq + texto, conteudo);

            if (escreveu) {
                System.out.println("\nEscrita OK!\n");

                EditorDeTexto.ListContent(path);

                System.out.println("\nDigite o nome do arquivo que deseja deletar:\n");
                deleteArq = scan.nextLine();
                boolean excluiu = EditorDeTexto.deleteFile(path + deleteArq + texto);
                if (excluiu) {
                    System.out.println("\n Arquivo " + deleteArq + texto + " excluido com sucesso!\n");
                    EditorDeTexto.ListContent(path);
                } else {
                    System.out.println("\nFalha ao excluir arquivo " + deleteArq + "!\n");
                }
            } else {
                System.out.println("\nERRO: ESCRITA NOT OK!\n");
            }
        } else {
            System.out.println("\nERRO: " + "Não foi possível criar o arquivo!\n");
        }

        scan.close();

    }
}