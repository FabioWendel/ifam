package app.ProvaLPOO;

public class ItemCompra {

    private int idProduto;
    private double valor;
    private int quantidade;

    public int getIdProduto() {
        return idProduto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public double calcularValorTotal() {
        
        return quantidade *valor ;
        
    }

}