package app;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Explorer {
    // private String patch;

    // public Explorer (String patch){
    // this.patch = patch;
    // }

    public static void ListContent(String patch) {
        File dir = new File(patch);

        if (dir.exists() && dir.isDirectory()) {
            File[] conteudoDiretorio = dir.listFiles();

            for (File file : conteudoDiretorio) {
                System.out.print("=>" + file.getName());
                if (file.isDirectory()) {
                    System.out.println(" (pasta) ");
                    ListContent(file.getPath());
                } else {
                    System.out.println(" (arquivo) ");
                }
            }
        }
    }

    public static boolean createFile(String pathFile) {

        try {
            File file = new File(pathFile);
            if (file.exists()) {
                return false;
            } else {
                file.createNewFile();
                return true;
            }
        } catch (IOException e) {
            System.out.println("Erro ao criar o arquivo: " + e.toString());
            return false;
        }
    }

    public static boolean writeFile(String pathFile, String text) {
        try {
            File file = new File(pathFile);
            if (file.exists()) {
                PrintWriter fw = new PrintWriter(file);
                fw.println(text);
                fw.close();
            }
            return true;
        } catch (IOException e) {
            System.out.println("Erro ao abrir o arquivo: " + e.toString());
            return false;
        }
    }

}