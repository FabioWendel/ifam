package app;

public class ExecExplorer {
    public static void main(String[] args) {
        // Explorer explorer = new Explorer();
        String path = "/home/fabio/ArquivosLPOO/";
        Explorer.ListContent(path);
        boolean criou = Explorer.createFile(path + "teste2.txt");

        if (criou) {
            System.out.println("Arquivo Criado!");
        } else {
            System.out.println("ERRO: " + "Não foi possível criar o arquivo!");
        }

        boolean escreveu = Explorer.writeFile(path + "teste2.txt", "!!AULA TOP POO!!");

        if (escreveu) {
            System.out.println("Escrita OK!");
        } else {
            System.out.println("ERRO: ESCRITA NOT OK!");
        }
    }
}