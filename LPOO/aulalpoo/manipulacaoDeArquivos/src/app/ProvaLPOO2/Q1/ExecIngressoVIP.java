package Q1;

import java.util.Scanner;

public class ExecIngressoVIP {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        Ingresso ingre = new Ingresso();

        IngressoVIP vip = new IngressoVIP();

        System.out.println("\nDigite o nome do seu evento:\n");
        vip.setEvento(scan.nextLine());
        System.out.println("\nDigite o valor do ingresso:\n");
        vip.setValor(scan.nextDouble());
        System.out.println(vip.getValor());
        System.out.println("\nValor adicional do ingresso Vip:\n");
        vip.setValorAdicional(scan.nextDouble());
        System.out.println("\nValor total do ingresso para o evento "
                + vip.getEvento() + " é : " + vip.calcularValorIngressoVIP());

    }
}
