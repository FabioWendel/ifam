package app.ProvaLPOO2.Q3;

import java.util.Scanner;

public class ExecFormaGeometrica {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        FormaGeometrica quadrado = new Quadrado();

        FormaGeometrica triangulo = new Triangulo();

        System.out.println("\n Digite lado do quadrado:\n ");
        quadrado.setLado(scan.nextDouble());

        System.out.println("\n Digite base do triangulo:\n ");
        triangulo.setBase(scan.nextDouble());
        System.out.println("\n Digite altura do triangulo:\n ");
        triangulo.setAltura(scan.nextDouble());

        System.out.println("\n Área do quadrado : " + quadrado.calcularArea());

        System.out.println("\n Área do triangulo : " + triangulo.calcularArea());
    }

}