package Q2;

import java.util.List;

public class Aluno {

    private String matricula;
    private String nome;
    private List<Double> listaNotas;

    public matricula(){

    }

    public nome(){
        
    }

    public Aluno(String matricula, String nome) {
        this.matricula = matricula;
        this.nome = nome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Double> getListaNotas() {
        return listaNotas;
    }

    public void setListaNotas(List<Double> listaNotas) {
        this.listaNotas = listaNotas;
    }

    void incluirNota(double nota) {
            listaNotas.add(nota);
    }

    void listarNotas() {
        for (Double listaNota : listaNotas) {
            System.out.println(""+listaNota);
        }
    }

}
