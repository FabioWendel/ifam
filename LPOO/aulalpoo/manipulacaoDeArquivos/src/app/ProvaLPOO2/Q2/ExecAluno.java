package Q2;

import java.util.Scanner;

public class ExecAluno {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
    
        Aluno aluno = new Aluno();

        System.out.println("\nInserir nome do aluno:\n");
        aluno.setNome(scan.nextLine());
        System.out.println("\nInserir matricula do aluno:\n");
        aluno.setMatricula(scan.nextLine());
        System.out.println("\nInserir notas do aluno:\n");
        aluno.incluirNota(scan.nextDouble());

        listarNotas();
    }
}
