/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Q3;

import java.util.Scanner;

/**
 *
 * @author Aluno
 */
public class ExecFormaGeometrica {
        public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        Quadrado quadrado = new Quadrado();

        Triangulo triangulo = new Triangulo();

        System.out.println("\n Digite lado do quadrado:\n ");
        quadrado.setLado(scan.nextDouble());

        System.out.println("\n Digite base do triangulo:\n ");
        triangulo.setBase(scan.nextDouble());
        System.out.println("\n Digite altura do triangulo:\n ");
        triangulo.setAltura(scan.nextDouble());

        System.out.println("\n Área do quadrado : " + quadrado.calcularArea());

        System.out.println("\n Área do triangulo : " + triangulo.calcularArea());
    }
}
