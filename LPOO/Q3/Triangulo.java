/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Q3;

/**
 *
 * @author Aluno
 */

public class Triangulo implements FormaGeometrica{
	
	private double base;
	private double altura;
	
	
	@Override
	public double calcularArea() {
		double areaTA = (this.base * this.altura/2);
		return areaTA;
	}


	public double getBase() {
		return base;
	}


	public void setBase(double base) {
		this.base = base;
	}


	public double getAltura() {
		return altura;
	}


	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	

}