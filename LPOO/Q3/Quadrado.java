/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Q3;

/**
 *
 * @author Aluno
 */
public class Quadrado implements FormaGeometrica {

	private double lado;

	@Override
	public double calcularArea() {
		double areaQD = this.lado * this.lado;
		return areaQD;
	}

	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}

}
