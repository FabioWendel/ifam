/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package P1Q7;

import java.util.Scanner;

/**
 *
 * @author Aluno
 */
public class ExecAluno {

    public static void main(String[] args) {
        
        Scanner scan = new Scanner(System.in);

        Aluno aluno = new Aluno();

        double listaNotas[] = new double[3];
        
        System.out.println("\nInforme o nome do aluno: ");
        aluno.setNome(scan.nextLine());

        for (int i = 0; i < 3; i++) {
            System.out.println("\nInforme a nota do aluno: ");
            listaNotas[i] = scan.nextDouble();

        }

        aluno.setNotas(listaNotas);
        if (aluno.passou(aluno.CalcMedia())) {
            System.out.println("\nAluno aprovado");
        } else {
            System.out.println("\nAluno reprovado");
        }

    }

}
