/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package P1Q7;

/**
 *
 * @author Aluno
 */
public class Aluno {

    private String nome;
    private double[] notas;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double[] getNotas() {
        return notas;
    }

    public void setNotas(double[] notas) {
        this.notas = notas;
    }

    public double CalcMedia() {
        double nota = 0;
        double media = 0;
        for (int i = 0; i < notas.length; i++) {
            nota = nota + notas[i];

        }
        media = nota /  notas.length;
        return media;
    }

    public boolean passou(double media) {

        if (media >= 6.0) {
            return true;
        } else {
            return false;

        }

    }
}
