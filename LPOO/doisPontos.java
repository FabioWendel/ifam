import java.text.DecimalFormat;
import java.util.Scanner;

class Main {
    public static void main(String [] args){
        Scanner ler = new Scanner(System.in);
        double x1,x2,y1,y2,raiz,a,b;
        x1=ler.nextDouble();
        y1=ler.nextDouble();
        x2=ler.nextDouble();
        y2=ler.nextDouble();
        a =Math.pow ((x2-x1),2);
        b =Math.pow ((y2-y1),2);
        raiz = Math.sqrt(a+b);
        DecimalFormat df = new DecimalFormat("#0.0000");
        System.out.println(df.format(raiz));
    }
}
