/* 	Cenario do problema: Jogo de Dominó
	28pedras
	Metodos:
		1) Inicializar o jogo: Inicializar();
		2) Jogar(N numero do jogador = 4, lado1, lado2); 
			Ex: Jogador(1,6,6);//Inicial =lado1==lado2==6
			2°Etapa/2°Rodada
			Jogar(1,0,0);//Inicio etapa seguinte lado1==lado2
		3)Ponto(n° Jogador, Pontuação);
			Ex: Ponto(1,5);
*/
#include <stdio.h>
#include <stdlib.h>

typedef struct peca
{
	int lado1;
	int lado2;
	int j;
	peca *prox;
} Pedra;

Pedra *Inicio = NULL;
Pedra *ult;
Pedra *tmp;

void CriarLista()
{
	Inicio = NULL;
}

void InserirEsq(int j, int v1, int v2)
{
	Pedra *p;
	p = (Pedra *)malloc(sizeof(Pedra));
	if (p == NULL)
	{
		printf("\nERRO: Falta de memoria!\n");
		return;
	}
	p->lado1 = v1;
	p->lado2 = v2;
	p->j = j;
	p->prox = NULL;

	if (Inicio == NULL)
	{
		Inicio = p;
	}
	else
	{
		tmp = Inicio;
		Inicio = p;
		Inicio->prox = tmp;
	}
}

void InserirDir(int j, int v1, int v2)
{
	Pedra *p;
	p = (Pedra *)malloc(sizeof(Pedra));
	if (p == NULL)
	{
		printf("\nERRO: Falta de memoria!\n");
		return;
	}
	else
	{
		p->lado1 = v1;
		p->lado2 = v2;
		p->j = j;
		p->prox = NULL;

		if (Inicio == NULL)
		{
			Inicio = p;
			ult = Inicio;
		}
		else
		{
			ult->prox = p;
			ult = p;
		}
	}
}

void Imprimir()
{
	Pedra *p;
	if (Inicio == NULL)
	{
		printf("\nLista Vazia\n");
		return;
	}
	else
	{
		p = Inicio;
		printf("\n\t\tDominor - V0.0.1\n\n");

		while (p != NULL)
		{
			printf("[%d:%d]", p->lado1, p->lado2);
			p = p->prox;
		};
	}
}

main()
{
	printf("\n\t\tDominor - V0.0.1\n\n");
	int v1, v2, op, j = 1;

	while (1)
	{
		if (Inicio == NULL)
		{
			printf("\n Jogador %d primeira pedra: ", j);
			scanf("%d %d", &v1, &v2);
			InserirDir(j, v1, v2);
		}
		else
		{
			printf("\n Jogador %d:\n Escolha pra que lado jogar a pedra Esquerdo(1) ou Direito(2): ", j);
			scanf("%d", &op);
			switch (op)
			{
			case 1:
				printf("\n Jogador %d: Jogue a proxima pedra: ", j);
				scanf("%d %d", &v1, &v2);
				if (ult->lado1 == v2)
				{
					InserirEsq(j, v1, v2);
					break;
				}
				else
				{
					printf("\n valor invalido, tente novamente");
					break;
					op = 1;
				}
			case 2:
				printf("\n Jogador %d: Jogue a proxima pedra: ", j);
				scanf("%d %d", &v1, &v2);
				InserirDir(j, v1, v2);
				break;
			default:
				printf("Valor invalido");
				break;
			}
		}
		j++;
		if (j == 5)
		{
			j = 1;
		}
		system("clear");
		Imprimir();
	}
}