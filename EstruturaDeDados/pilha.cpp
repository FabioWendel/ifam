#include <stdio.h>
#include <stdlib.h>
#define max 10

void empilhar (float pilha[], float valor, int&topo, int tamanho){
    if( topo == tamanho){
        printf("\nErro pilha cheia");
    }else{
        pilha[topo] = valor;

        printf("\n Empilhado pilha[%d]=%j\n", topo, valor);
        topo++;
    }
};
void exibir (float pilha[], int topo){
    if(topo == 0){
        printf("\nErro pilha vazia");
    }else{
        for(int i = (topo-1); i>0 ; i--){
            printf("\n Pilha[i]=%f\n", i , pilha[i]);
        }
    }
};
void desempilhar_topo (float pilha[], int &topo){
    if(topo == 0){
        printf("\n Erro pilha vazia");
    }else{
        pilha[topo-1] = 0;
        topo--;
        printf("\n Desempilhando o topo\n");
    }
};

int main (){
    float pilha[max];

    int topo =0;

    for (int i =0; i< max; i++){
        empilhar(pilha, 1.1, topo, max);
    }

    exibir(pilha, topo);

    desempilhar_topo(pilha, topo);
    exibir(pilha, topo);

}
