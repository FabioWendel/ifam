#include<stdio.h>
#include<stdlib.h>

typedef struct no{
	int info;
	no *prox;
}Tno;

Tno *Inicio = NULL;
Tno *ult;
Tno*aux;

void CriarLista(){
	Inicio = NULL;
}

void Inserir(int dado){
	Tno *p;
	p = (Tno *)malloc(sizeof(Tno));
	if(p==NULL){
		printf("\nERRO: Falta de memoria!\n");
		return;
	}
	p->info = dado;
	p->prox = NULL;
	
	if(Inicio==NULL){
		Inicio = p;
	}else{
		Inicio->prox = Inicio;
		Inicio->prox = p;
	}
}

void RemoveInicio(){
	Tno *p;
	if(Inicio==NULL){
		printf("Lista Vazia");
		return;
	}else{
		p = Inicio;
		Inicio = p->prox;
		p->prox = NULL;
		free(p);
	}
}
void RemoveFim(){
	Tno *p;
	if(Inicio==NULL){
		printf("Lista Vazia");
		return;
	}else{
		aux = ult;
		ult = ult->prox;
		ult->prox = NULL;
		free(ult);
	}
}

void InserirFim(int dado){
	Tno *p;
	p = (Tno *)malloc(sizeof(Tno));
	if(p==NULL){
		printf("\nERRO: Falta de memoria!\n");
		return;
	}else{
		p->info = dado;
		p->prox = NULL;
		if(Inicio==NULL){
			Inicio = p;
			ult = Inicio;
		}else{
			ult->prox = p;
			ult = p;
		}
	}
}

void Imprimir(){
	Tno *p;
	if(Inicio==NULL){
		printf("\nLista Vazia\n");
		return;
	}else{
		p = Inicio;
		while(p!=NULL){
			printf("\nValor = %d",p->info);
			p = p->prox;
		};
	}
	printf("\nFim de lista");
}



main(){
	//Inserir(10);
	//Inserir(20);
	InserirFim(4);
	InserirFim(3);
	InserirFim(2);
	InserirFim(1);
	RemoveFim();
//	RemoveInicio();
	Imprimir();
}
