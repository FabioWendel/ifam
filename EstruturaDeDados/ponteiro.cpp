//ponteiro � um recurso para elabora��o de algoritmos  que manipulam mem�ria dinamicamente;
//o ponteiro armazena endere�o de mem�ria;
//declara um ponteiro --->>> int *ponteiro;
//operadores para manipular ponteiros (& / *) 
//'&'-->> obter o endere�o de mem�ria de uma vari�vel
//'*'-->> acessar o conte�do armazenado em um endere�o de mem�ria  
//obs: o pontteiro tem que ser do mesmo tipo do conte�do da mem�ria 
//fun��es de stdlib.h
//'sizeof(variavel)'-->> tamanho da variavel
//'malloc(tamanho)'-->> aloca uma area de mem�ria para um ponteiro
//'free(ponteiro)'-->> libera a area de mem�ria alocada 
//Exemplo:
#include <stdlib.h>
#include <stdio.h>

int main(){
	/*
	int valor;
	
	printf("%x \n", &valor);
	
	printf("\ninserir qualquer valor inteiro:");
	scanf("%d", &valor);
	
	int *ponteiro;
	ponteiro = &valor;
	printf("\nvariavel valor=%d\n", valor);
	printf("\nconteudo do ponteiro=%d\n", *ponteiro);
	
	*ponteiro = 2*(*ponteiro);
	
	printf("\nnovo valor da variavel=%d\n");
	*/
	
	typedef struct Tvalores{
		int valor;
		Tvalores *prox;
	}Tvalor;
	
	//exemplo com valores estaticos
	/*Tvalor preco_produto;
	preco_produto.valor = 10;
	Tvalor *preco_variavel;
	
	preco_variavel = &preco_produto;
	preco_variavel->valor = 20;*/
	
	
	//exemplo com valores estaticos
	/*Tvalor preco_produto;
	Tvalor *preco_variavel;
	preco_variavel = (Tvalor*)malloc(sizeof(Tvalor));
	
	preco_produto.valor = 10;
	
	preco_variavel = &preco_produto;
	
	preco_variavel->valor = 20;*/
	
	Tvalor *lista_valores;
	Tvalor *aux;
	lista_valores=(Tvalor*)malloc(sizeof(Tvalor));
	lista_valores->valor = 10;
	printf("\nValor = %d\n",lista_valores->valor);
	lista_valores->prox = NULL;
	lista_valores->prox = (Tvalor*)malloc(sizeof(Tvalor));
	//lista_valores->prox->valor = 20;
	aux = lista_valores->prox; // variavel aux para nao usar todo caminho pontero
	aux->valor = 20;
	
	
	printf("\nValor novo = %d\n",lista_valores->prox->valor);
}

