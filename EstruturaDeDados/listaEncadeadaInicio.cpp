#include <stdio.h>
#include <stdlib.h>

typedef struct no
{
    int info;
    no *prox;
} Tno;

Tno *inicio = NULL;
Tno *aux;
Tno *ult;

void CriarLista()
{
    inicio = NULL;
}

void InserirInicio(int dado)
{
    Tno *p;
    p = (Tno *)malloc(sizeof(Tno));
    if (p == NULL)
    {
        printf("\nERRO: Falta de memoria!\n");
        return;
    }
    p->info = dado;
    p->prox = NULL;

    if (inicio == NULL)
    {
        inicio = p;
    }
    else
    {
        p->prox = inicio;
        inicio = p;
    }
}

void InserirFim(int dado)
{
    Tno *p;
    p = (Tno *)malloc(sizeof(Tno));
    if (p == NULL)
    {
        printf("\nERRO: Falta de memoria!\n");
        return;
    }
    else
    {
        p->info = dado;
        p->prox = NULL;
        if (inicio == NULL)
        {
            inicio = p;
            ult = inicio;
        }
        else
        {
            ult->prox = p;
            ult = p;
        }
    }
}

void ImprimirLista()
{
    Tno *p;
    if (inicio == NULL)
    {
        printf("\nLista Vazia\n");
        return;
    }
    else
    {
        p = inicio;
        while (p != NULL)
        {
            printf("\nValor = %d", p->info);
            p = p->prox;
        };
    }
    printf("\nFim de lista\n");
}

int main()
{
    InserirFim(1);
    InserirFim(2);
    InserirFim(3);
    ImprimirLista();

    InserirInicio(4);
    InserirInicio(5);
    InserirInicio(6);
    ImprimirLista();
}